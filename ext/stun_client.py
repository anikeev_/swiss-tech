import stun


class StunClientException(Exception):
    pass


class StunClient(object):
    def __init__(self, supported_nat_types=(stun.FullCone,)):
        self.supported_nat_types = supported_nat_types
        self.address, self.port = self.get_ip_info()

    def get_ip_info(self):
        nat_type, address, port = stun.get_ip_info()
        if nat_type not in self.supported_nat_types:
            raise StunClientException('your nat type ({0}) is not supported yet. '
                                      'Only {1} supported now'.format(nat_type, self.supported_nat_types))

        return address, port

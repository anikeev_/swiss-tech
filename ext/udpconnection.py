from abc import abstractmethod

from twisted.internet import reactor
from twisted.internet.protocol import DatagramProtocol


class UdpConnection(DatagramProtocol):
    connect_to_addr = None
    connect_to_port = None
    on_datagram_received_callback = None

    def send_datagram(self, datagram):
        self.transport.write(datagram)

    def connect(self, addr, port):
        self.connect_to_addr = addr
        self.connect_to_port = port
        reactor.listenUDP(0, self)

    def listen(self, port):
        reactor.listenUDP(port, self)

    def startProtocol(self):
        if self.connect_to_addr and self.connect_to_port:
            self.transport.connect(self.connect_to_addr, self.connect_to_port)

    def datagramReceived(self, datagram, address):
        if self.on_datagram_received_callback is not None:
            self.on_datagram_received_callback(datagram, address)

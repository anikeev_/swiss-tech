from twisted.internet import reactor

import settings
from autobahn.twisted import WebSocketServerFactory
from autobahn.twisted.websocket import listenWS
from signal_server_connection import SignalServerConnection


class SignalServer(WebSocketServerFactory):
    def __init__(self, url):
        WebSocketServerFactory.__init__(self, url)
        self.handlers = dict(register=self._register_server, list=self._list_servers)
        self.servers = dict()

    def _register_server(self, peer, args):
        self.servers[peer] = args
        print 'server registered', peer, args

    def _list_servers(self, peer, args):
        return self.servers.values()

    def unregister_server(self, peer):
        self.servers.pop(peer, None)
        print 'server unregistered', peer

    def run(self):
        reactor.run()

if __name__ == '__main__':
    server = SignalServer(settings.SIGNAL_SERVER_ADDRESS)
    server.protocol = SignalServerConnection
    listenWS(server)
    server.run()

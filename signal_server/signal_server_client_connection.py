from autobahn.twisted import WebSocketClientProtocol


class SignalServerClientConnection(WebSocketClientProtocol):
    def onOpen(self):
        self.factory.on_connected_to_signal_server(self)

    def onMessage(self, payload, isBinary):
        if not isBinary:
            self.factory.on_message_from_signal_server(payload)

    def send_message(self, message):
        self.sendMessage(message, isBinary=False)

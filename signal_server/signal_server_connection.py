from autobahn.twisted.websocket import WebSocketServerProtocol
from twisted.internet.defer import Deferred, inlineCallbacks
import json

class SignalServerConnection(WebSocketServerProtocol):
    @inlineCallbacks
    def onMessage(self, payload, isBinary):
        if not isBinary:
            message = json.loads(payload.decode('utf8'))
            try:
                res = yield self.factory.handlers[message['request']](self.peer, message['args'])
                message['result'] = res
            except Exception, e:
                self.sendClose(1000, "Exception raised: {0}".format(e))
            else:
                self.sendMessage(json.dumps(message).encode('utf8'))

    def onConnectionLost(self, reason):
        self.factory.unregister_server(self.peer)

    def onClose(self, wasClean, code, reason):
        self.factory.unregister_server(self.peer)

import json

from twisted.internet import reactor

import settings
from autobahn.twisted import WebSocketClientFactory
from autobahn.twisted.websocket import connectWS
from ext.udpconnection import UdpConnection
from signal_server.signal_server_client_connection import SignalServerClientConnection

class ClientException(Exception):
    pass

class Client(WebSocketClientFactory):
    def __init__(self, signal_server_url, datagram):
        WebSocketClientFactory.__init__(self, url=signal_server_url)
        self.datagram = datagram
        self.udp = UdpConnection()

    def run(self):
        reactor.run()

    @staticmethod
    def stop():
        reactor.stop()

    def on_connected_to_signal_server(self, connection):
        connection.send_message(json.dumps(dict(request='list', args=dict())))

    def on_message_from_signal_server(self, message):
        message = json.loads(message)
        print 'message from signal server', message
        request = message['request']
        try:
            result = message['result'][0]
        except IndexError, e:
            raise ClientException('No servers registered on signal-server')
        address = result['address']
        port = result['port']

        if request == 'list':
            self.udp.connect(address, port)
            reactor.callLater(0, self.udp.send_datagram, self.datagram)

if __name__ == '__main__':
    datagram = str(raw_input('Input datagram:'))
    app = Client(settings.SIGNAL_SERVER_ADDRESS, datagram=datagram)
    app.protocol = SignalServerClientConnection
    connectWS(app)
    app.run()


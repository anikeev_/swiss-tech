import json
from datetime import datetime

import settings
from autobahn.twisted import WebSocketClientFactory
from autobahn.twisted.websocket import connectWS
from ext.stun_client import StunClient
from ext.udpconnection import UdpConnection
from twisted.internet import reactor

from signal_server.signal_server_client_connection import SignalServerClientConnection


class Server(WebSocketClientFactory):
    def __init__(self, signal_server_url):
        super(Server, self).__init__()
        WebSocketClientFactory.__init__(self, url=signal_server_url)
        self.udp = UdpConnection()
        self.udp.on_datagram_received_callback = self.on_datagram_received
        self.stun = StunClient()

    def run(self):
        self.udp.listen(self.stun.port)
        reactor.run()

    @staticmethod
    def stop():
        reactor.stop()

    def on_connected_to_signal_server(self, connection):
        connection.send_message(json.dumps(dict(request='register', args=dict(address=self.stun.address,
                                                                              port=self.stun.port))))

    def on_message_from_signal_server(self, message):
        pass

    def on_datagram_received(self, datagram, address):
        print '{0} : {1} : {2}'.format(datetime.now(), str(address), repr(datagram))


if __name__ == '__main__':
    app = Server(settings.SIGNAL_SERVER_ADDRESS)
    app.protocol = SignalServerClientConnection
    connectWS(app)
    app.run()

